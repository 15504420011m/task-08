package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class FlowerHandler extends DefaultHandler {
    private List<Flower> flowers;
    private Flower curFlower;
    private String curElement;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        curElement = qName;
        if (qName.equals("flowers")) {
            flowers = new ArrayList<>();
        }else if (qName.equals("flower")) {
            curFlower = new Flower();
        } else if(qName.equals("lighting")){
            curFlower.setLightRequiring(attributes.getValue("lightRequiring"));
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(qName.equals("flower")){
            flowers.add(curFlower);
        }
    }

    public List<Flower> getFlowers (){
        return flowers;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String elementValue = new String(ch,start, length).trim();
        if(elementValue.length() == 0)return;
        switch (curElement) {
            case "name":
                curFlower.setName(elementValue);
                break;
            case "soil":
                curFlower.setSoil(elementValue);
                break;
            case "origin":
                curFlower.setOrigin(elementValue);
                break;
            case "stemColour":
                curFlower.setStemColour(elementValue);
                break;
            case "leafColour":
                curFlower.setLeafColour(elementValue);
                break;
            case "aveLenFlower":
                curFlower.setAveLenFlower(Integer.parseInt(elementValue));
                break;
            case "temperature":
                curFlower.setTemperature(Integer.parseInt(elementValue));
                break;
            case "watering":
                curFlower.setWatering(Integer.parseInt(elementValue));
                break;
            case "multiplying":
                curFlower.setMultiplying(elementValue);
                break;
        }
    }
}
