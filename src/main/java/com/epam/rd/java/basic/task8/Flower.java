package com.epam.rd.java.basic.task8;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String stemColour;
    private String leafColour;
    private int aveLenFlower;
    private int temperature;
    private String lightRequiring;
    private int watering;
    private String multiplying;

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public int getTemperature() {
        return temperature;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public int getWatering() {
        return watering;
    }

    public String getMultiplying() {
        return multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                ", temperature=" + temperature +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", watering=" + watering +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
