package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parseFlowers(){
		List<Flower> flowers = null;
		try {
			File file = new File(xmlFileName);
			SAXParserFactory f = SAXParserFactory.newInstance();
			FlowerHandler flowerHandler = new FlowerHandler();
			f.newSAXParser().parse(file, flowerHandler);
			flowers = flowerHandler.getFlowers();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return flowers;
	}

}