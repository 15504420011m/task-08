package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);


		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> DOMFlowers = domController.parseFlowers();
		// PLACE YOUR CODE HERE

		// sort (case 1)
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		XMLSaverController DOMSaver = new XMLSaverController(outputXmlFile, DOMFlowers);
		DOMSaver.createXML();// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		List<Flower> SaxFlowers = saxController.parseFlowers();
		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		XMLSaverController SAXSaver = new XMLSaverController(outputXmlFile, SaxFlowers);
		SAXSaver.createXML();
		// PLACE YOUR CODE HERE
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> StaxFlowers = staxController.parseFlowers();
		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		XMLSaverController StaxSaver = new XMLSaverController(outputXmlFile, StaxFlowers);
		StaxSaver.createXML();
		// PLACE YOUR CODE HERE
	}

}
