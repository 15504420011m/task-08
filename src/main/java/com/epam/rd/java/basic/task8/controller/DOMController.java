package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	private Document buildDOM(){
		Document doc = null;
		try {
			File file = new File(xmlFileName);
			DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
			doc = f.newDocumentBuilder().parse(file);
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
			return doc;
	}

	public List<Flower> parseFlowers (){
		Document doc = buildDOM();
		List<Flower> flowers= new ArrayList<>();
		Element root = doc.getDocumentElement();
		NodeList nodeList = root.getElementsByTagName("flower");

		for(int i = 0; i < nodeList.getLength(); i++){
			Element flowerElement = (Element) nodeList.item(i);
			flowers.add(buildFlower(flowerElement));
		}
		return flowers;
	}

	private Flower buildFlower(Element flowerElement){
		Flower flower = new Flower();
		flower.setName(flowerElement.getElementsByTagName("name").item(0).getTextContent());
		flower.setSoil(flowerElement.getElementsByTagName("soil").item(0).getTextContent());
		flower.setOrigin(flowerElement.getElementsByTagName("origin").item(0).getTextContent());

		Element visualParametersEl = (Element) flowerElement.getElementsByTagName("visualParameters").item(0);
		flower.setStemColour(visualParametersEl.getElementsByTagName("stemColour").item(0).getTextContent());
		flower.setLeafColour(visualParametersEl.getElementsByTagName("leafColour").item(0).getTextContent());
		flower.setAveLenFlower(Integer.parseInt(visualParametersEl.getElementsByTagName("aveLenFlower").item(0).getTextContent()));

		Element growingTipsEl = (Element) flowerElement.getElementsByTagName("growingTips").item(0);
		flower.setTemperature(Integer.parseInt(growingTipsEl.getElementsByTagName("temperature").item(0).getTextContent()));
		flower.setLightRequiring(((Element) growingTipsEl.getElementsByTagName("lighting").item(0)).getAttribute("lightRequiring"));
		flower.setWatering(Integer.parseInt(growingTipsEl.getElementsByTagName("watering").item(0).getTextContent()));

		flower.setMultiplying(flowerElement.getElementsByTagName("multiplying").item(0).getTextContent());
		return flower;
	}

}
