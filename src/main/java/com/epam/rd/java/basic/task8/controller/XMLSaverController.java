package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class XMLSaverController {
    private String targetName;
    private List<Flower> flowers;
    private Document document;

    public XMLSaverController(String targetName, List<Flower> flowers) {
        this.targetName = targetName;
        this.flowers = flowers;
    }

    public void createXML() throws ParserConfigurationException {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        document = documentBuilder.newDocument();
        Element root = document.createElement("flowers");
        root.setAttribute("xmlns", "http://www.nure.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.nure.ua.target/input.xsd");
        document.appendChild(root);
        for(Flower flower: flowers){
            Element flowerEl = document.createElement("flower");
            createFlower(flower, flowerEl);
            root.appendChild(flowerEl);
        }
        transformDocument();
    }

    private void createFlower(Flower flower, Element flowerEl){
        Element name = document.createElement("name");
        name.appendChild(document.createTextNode(flower.getName()));
        flowerEl.appendChild(name);

        Element soil = document.createElement("soil");
        soil.appendChild(document.createTextNode(flower.getSoil()));
        flowerEl.appendChild(soil);

        Element origin = document.createElement("origin");
        origin.appendChild(document.createTextNode(flower.getOrigin()));
        flowerEl.appendChild(origin);


        Element params = document.createElement("visualParameters");

        Element stemColour = document.createElement("stemColour");
        stemColour.appendChild(document.createTextNode(flower.getStemColour()));
        params.appendChild(stemColour);

        Element leafColour = document.createElement("leafColour");
        leafColour.appendChild(document.createTextNode(flower.getLeafColour()));
        params.appendChild(leafColour);

        Element aveLenFlower = document.createElement("aveLenFlower");
        aveLenFlower.setAttribute("measure","cm");
        aveLenFlower.appendChild(document.createTextNode(String.valueOf(flower.getAveLenFlower())));
        params.appendChild(aveLenFlower);
        flowerEl.appendChild(params);


        Element tips = document.createElement("growingTips");
        Element temperature = document.createElement("temperature");
        temperature.setAttribute("measure","celcius");
        temperature.appendChild(document.createTextNode(String.valueOf(flower.getTemperature())));
        tips.appendChild(temperature);

        Element lightning = document.createElement("lighting");
        lightning.setAttribute("lightRequiring", flower.getLightRequiring());
        tips.appendChild(lightning);

        Element watering = document.createElement("watering");
        watering.setAttribute("measure","mlPerWeek");
        watering.appendChild(document.createTextNode(String.valueOf(flower.getWatering())));
        tips.appendChild(watering);
        flowerEl.appendChild(tips);

        Element multiplying = document.createElement("multiplying");
        multiplying.appendChild(document.createTextNode(flower.getMultiplying()));
        flowerEl.appendChild(multiplying);
    }

    private void transformDocument(){
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new FileWriter(targetName));
            transformer.transform(domSource, streamResult);
        } catch (IOException | TransformerException e) {
            e.printStackTrace();
        }
    }

}
