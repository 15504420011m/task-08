package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;
    private XMLStreamReader reader;


    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> parseFlowers() {
        List<Flower> flowers = new ArrayList<>();
        try (InputStream input = new FileInputStream(xmlFileName)) {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            reader = factory.createXMLStreamReader(input);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    String name = reader.getLocalName();
                    if (name == "flower") {
                        flowers.add(buildFlower());
                    }
                }
            }
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
        return flowers;
    }

    private Flower buildFlower() throws XMLStreamException {
        Flower flower = new Flower();
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    String el = reader.getLocalName();
                    switch (el) {
                        case "name":
                            flower.setName(getXmlText());
                            break;
                        case "soil":
                            flower.setSoil(getXmlText());
                            break;
                        case "origin":
                            flower.setOrigin(getXmlText());
                            break;
                        case "stemColour":
                            flower.setStemColour(getXmlText());
                            break;
						case "leafColour":
							flower.setLeafColour(getXmlText());
							break;
						case "aveLenFlower":
							flower.setAveLenFlower(Integer.parseInt(getXmlText()));
							break;
						case "temperature":
							flower.setTemperature(Integer.parseInt(getXmlText()));
							break;
						case "lighting":
							flower.setLightRequiring(reader.getAttributeValue(0));
							break;
						case "watering":
							flower.setWatering(Integer.parseInt(getXmlText()));
							break;
						case "multiplying":
							flower.setMultiplying(getXmlText());
							break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    String element = reader.getLocalName();
                    if (element == "flower") {
                        return flower;
                    }
            }
        }
        throw new XMLStreamException("Cannot find end flower element");
    }

    private String getXmlText() throws XMLStreamException {
        String text = null;
        if(reader.hasNext()){
            reader.next();
            text = reader.getText();
        }
        return text;
    }
    // PLACE YOUR CODE HERE

}